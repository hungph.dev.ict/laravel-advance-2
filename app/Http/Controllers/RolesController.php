<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as AppController;
use \App\Models as Database;

class RolesController extends AppController
{
    /**
     * Constructor
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * List role
     *
     * /roles
     */
    public function index()
    {
        $roles = Database\Role::get();

        return view('roles.index', compact('roles'));
    }
}
