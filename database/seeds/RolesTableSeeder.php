<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('roles')->truncate();
        Schema::enableForeignKeyConstraints();

        $roles = [
            'Quản trị cấp cao',
            'Nhân viên',
        ];

        foreach ($roles as $role) {
            factory(App\Models\Role::class)->create([
                'name' => $role,
            ]);
        }
    }
}
