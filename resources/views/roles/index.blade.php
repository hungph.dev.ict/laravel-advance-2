@extends('layouts.main')
@section('title', __('Quản lý vai trò'))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="btn-link panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{{ __('▼　Danh sách') }}</h3>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>{{ __('STT') }}</th>
                        <th>{{ __('Tên') }}</th>
                        <th>{{ __('Người dùng') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->users->count() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop