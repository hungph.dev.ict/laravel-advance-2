<?php $querystring = app('request')->all(); ?>

@if ($records->lastPage() != 1)
    <ul class="pagination">
        <li class="{{ ($records->currentPage() == 1) ? 'disabled' : '' }}"
            style="{{ ($records->currentPage() == 1) ? 'pointer-events: none;' : '' }}" >
            <a href="{{ $records->appends($querystring)->url($records->currentPage() - 1) }}">{{ __('Prev') }} </a>
        </li>
        @for ($i = 1; $i <= $records->lastPage(); $i ++)
            <li class="{{ ($records->currentPage() == $i) ? 'active' : '' }}">
                <a href="{{ $records->appends($querystring)->url($i) }}">{{ $i }}</a>
            </li>
        @endfor
        <li class="{{ ($records->currentPage() == $records->lastPage()) ? 'disabled' : '' }}"
            style="{{ ($records->currentPage() == $records->lastPage()) ? 'pointer-events: none;' : '' }}">
            <a href="{{ $records->appends($querystring)->url($records->currentPage() + 1) }}">{{ __('Next') }}</a>
        </li>
    </ul>
@endif